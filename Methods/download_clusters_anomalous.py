import requests, sys
import csv
from Bio import SeqIO, Seq, SeqRecord
from Bio.Blast import NCBIXML
from io import StringIO
import subprocess

TOPLEVEL_URL = "https://uniprot.org"

# Helper function to download data
def get_url(url, **kwargs):
  response = requests.get(url, **kwargs);

  if not response.ok:
    print(response.text)
    response.raise_for_status()
    sys.exit()

  return response

# import the TSV of clusters and members
uniref_dict = {}
selection = ["UniRef90_P03631", "UniRef90_P62090", "UniRef90_B8XV92", "UniRef90_P07003", "UniRef90_A0A0A2VU13", "UniRef90_I0CMC8", "UniRef90_A0A177D7C1", "UniRef90_P0A698"]
# "UniRef90_P27634"
with open("/Users/martin/Internship/WesterdijkProject/Uniref90/uniref90_100clusters_metadata_1.tsv") as f:
    table = csv.reader(f, delimiter = "\t")
    for line in table:
        # select the names of the anomalous clusters 
        # if line[0] == "UniRef90_I0CMC8" or line[0] == "UniRef90_A0A177D7C1" or line[0] == "UniRef90_P0A698":
        #if line[0] == "UniRef90_A0A177D7C1":
        if line[0] in selection:
            memberslist = line[4].split("; ")
            uniref_dict[line[0]] = memberslist # store cluster ID and members as key-value pairs


# make cluster ID's iterable 
clusterIDs = list(uniref_dict.keys())

# loop through the cluster ID's
for cluster in clusterIDs:
    print(cluster) # prints the cluster ID
    
    members = uniref_dict[cluster] # list of sequences to fetch

    sequences = ''

    # download the sequences
    for acc in members:
        if acc.startswith("UPI"):
            db = "uniparc"
        else: db = "uniprot"
        r = get_url(f"https://uniprot.org/{db}/{acc}&format=fasta")
        seq = r.text
        #print(seq)
        sequences += seq

    # create Biopython SeqRecord object from sequences for easy handling
    fasta_io = StringIO(sequences)
    cluster_records = list(SeqIO.parse(fasta_io, "fasta"))
    fasta_io.close()

    # find longest one, seed sequence
    max_len = 0
    for record in cluster_records:
        if len(record.seq) > max_len:
            max_len = len(record.seq)
            seed = record

    # Generate database from seed sequence
    seedpath = "/Users/martin/Internship/WesterdijkProject/output/blastseed/seed.fasta"
    dbname = '/Users/martin/Internship/WesterdijkProject/output/blastseed/seeddb'
    with open(seedpath, "w") as outfile:
        SeqIO.write(seed, outfile, "fasta")
    # make a blast database using that file
    mk_blastdb = subprocess.run(['makeblastdb', '-in', seedpath, '-dbtype', 'prot', '-parse_seqids', '-out', dbname])
    
    # generate query file (all sequences, except the seed)
    querypath = "/Users/martin/Internship/WesterdijkProject/output/blastquery/query.fasta"
    with open(querypath, "w") as f:
        for record in cluster_records:
            if record.description == seed.description:
                continue # skip the seed sequence
            SeqIO.write(record, f, "fasta")

    # run Blastp with evalue cutoff 0.1 and max_hsps = 1
    resultpath = "/Users/martin/Internship/WesterdijkProject/output/blastquery/result.xml"
    # -outfmt 5 generates the output as XML
    blastp_result = subprocess.run(['blastp', '-query', querypath, '-db', dbname, '-matrix', 'IDENTITY', '-outfmt', '5', '-out', resultpath, '-evalue', '0.1', 'max_hsps', '1'])
    # blastp_result = subprocess.run(['blastp', '-query', querypath, '-db', dbname, '-matrix', 'IDENTITY', '-outfmt', '5', '-out', resultpath])
    
    # parse blastp output XML
    blast_result = open(resultpath)
    blast_records = list(NCBIXML.parse(blast_result))
    blast_result.close()

    # extract and store blastp results 
    blast_records = list(blast_records)
    results_lst = []
    for record in blast_records:
        print("hello")
        for alignment in record.alignments:
            print(vars(alignment))
            #print(dir(alignment))
            accession = alignment.accession
            for hsp in alignment.hsps:
                print(vars(hsp))
                #print(dir(hsp))
                # sequence length
                seq_len = hsp.align_length
                print(seq_len)
                # identical residues
                id_num = hsp.identities
                print(id_num)
                ratio = id_num / seq_len # similarity measure
                print(ratio)
                # number of undefined (X) residues
                x_res = 0
                for residue in hsp.query:
                    if residue == "X" or residue == "x":
                        x_res += 1
                
                
                result = [seq_len, id_num, ratio, x_res]
                results_lst.append(result)

    # save results to file
    outpath = "/Users/martin/Internship/WesterdijkProject/output/cluster_similarity_detailed/" + cluster + "_NEW_perseq_results_max_hsp_1.txt"
    with open(outpath, 'w') as fout:
        output_tsv = csv.writer(fout, delimiter = '\t')
        for r in results_lst:
            output_tsv.writerow(r)

#print(results_lst)






    

