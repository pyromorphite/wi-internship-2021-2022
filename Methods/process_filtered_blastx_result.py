import pandas
from iteration_utilities import duplicates
from collections import Counter

# import filtered blastx tsv results file
resultspath = "/Users/martin/Internship/WesterdijkProject/output/blastx/filtered/GCF_000002495_results_1500_filtered.txt"
colnames = ["query", "hit", "pct ID", "ID cnt", "length", "query len", "hit len", "query start", "query end", "hit start", "hit end", "evalue", "bitscore"]
blastx_filtered = pandas.read_csv(resultspath, sep = '\t', header = None, names = colnames)

# for each ORF, get which cluster is the top hit 
hits_list = list(blastx_filtered["hit"])
# for each ORF, store the associated uniref90 cluster
cols = ["query", "hit"]
orf_cluster_filepath = "/Users/martin/Internship/WesterdijkProject/output/blastx/filtered/metadata/GCF_000002495_1500_ORF_hits.txt"
blastx_filtered.to_csv(orf_cluster_filepath, columns = cols, index = False)

# For each cluster, the number of matching ORFs (Counter object is subclass of dictionary)
duplicate_counts = Counter(hits_list) 
# output a file storing the number of times each cluster was recorded 
duplicates_outf = "/Users/martin/Internship/WesterdijkProject/output/blastx/filtered/metadata/GCF_000002495_1500_duplicates.txt"
with open(duplicates_outf, 'w') as outf:
    for key in duplicate_counts.keys():
        line = key + "," + str(duplicate_counts[key]) + "\n"
        outf.write(line)

repspath = "/Users/martin/Internship/Clustering/uniref90_fungal_representatives_clusternames.txt"
representatives = []
with open(repspath, "r") as f:
    representatives = f.readlines()
    representatives = [x.strip() for x in representatives]

# for each of the ~8 million representative sequences, store if it represented in the genome's ORFs (1) or not (0)
# create empty dataframe with the representatives as rownames, and a column for 'presence in genome'. Initiated as 0's.
cluster_presence = pandas.DataFrame(0, index = representatives, columns = ["present"]) #, "count"])
#print(cluster_presence)

# iterate through representatives
print(cluster_presence)
cnt = 0
for rep in representatives:
    # check if the cluster appears in the hits_list
    # if it does, change the value of the present column to a 1. 
    if rep in hits_list:
        cnt += 1
        print(cnt)
        cluster_presence.at[rep, "present"] = 1
        #cluster_presence.at[rep, "count"] = duplicate_counts[rep] 

cluster_presence.to_csv('/Users/martin/Internship/WesterdijkProject/output/blastx/filtered/metadata/GCF_000002495_1500_clusterbool.txt')


