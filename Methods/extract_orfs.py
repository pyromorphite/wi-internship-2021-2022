import subprocess # run cli's
import os # to delete uncompressed genome at the end of the procedure

# import list of filepaths in Genomes directory
with open("output/Genomes_LinuxPaths.txt") as f:
    paths_list = f.readlines()
    paths_list = [x for x in paths_list if x.strip()] #removes empty lines or lines with only whitespace
    paths_list = [line.strip() for line in paths_list] #removes newline characters
    paths_list = [line.replace("/mnt/d/", "/Users/martin/Internship/") for line in paths_list]

for g in paths_list:
    #### create the name of the output file (genome_file)
    genome_file = "output/unpacked_genome.fna"
    #### unpack the genome and store it to genome_file
    with open(genome_file, "w") as f:
        # args -dkc mean: decompress, keep original file, send output to stdout
        # stdout redirected to genome_file
        subprocess.run(["gzip", "-dkc", g], stdout = f)
    #### create the name of the file with the orfs for this genome (orf_file.fasta)
    #### string starting after the final '/', structured as: 'GCF' + '_' + 9 digits (then a . and the version but the genomes are still findable without this)
    position = g.rfind("/")
    start = position + 1
    end = position + 14
    genome_name = g[start:end]
    print(genome_name)
    orf_file = "/Users/martin/Internship/ORFs_1500/" + genome_name + "_orfs.fna"
    print(orf_file)
    #### getorf -sequence genome_file outfile -outseq ORFs/orf_file.fasta
    subprocess.run(['getorf', '-sequence', genome_file, '-outseq', orf_file, '-minsize', '1500', '-find', '3'])
    #### delete the uncompressed genome_file, it's not needed anymore
    os.remove(genome_file)


