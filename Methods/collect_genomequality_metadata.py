# One by one take the genome quality files and store:
#### The N's per kb
#### The number of contigs

# print the full lists to a file

import os
resultsdir = "/Users/martin/Internship/WesterdijkProject/output/genome_quality"

Nlist = []
contigslist = []

for file in os.listdir(resultsdir):
    # the files with the results for each genome are .tsv format 
    if ".tsv" in file:
        path = os.path.join(resultsdir, file)
        #parse the results file and store the data to lists
        with open(path, 'r') as dataf:
            metadata = dataf.readlines()
            Ns = metadata[2].split(sep="\t")
            Nlist.append(Ns[1].strip())
            contigs = metadata[3].split(sep="\t")
            contigslist.append(contigs[1])

outpath = "/Users/martin/Internship/WesterdijkProject/output/genome_quality/summary.txt"
with open(outpath, 'w') as outf:
    for i, j in zip(Nlist, contigslist):
        line = i + "\t" + j + "\n" 
        outf.write(line)


    