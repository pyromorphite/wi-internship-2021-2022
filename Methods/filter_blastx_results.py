import pandas

#import results file
path = "/Users/martin/Internship/WesterdijkProject/output/blastx/GCF_000002495_results_1500.txt"
colnames = ["query", "hit", "pct ID", "ID cnt", "length", "query len", "hit len", "query start", "query end", "hit start", "hit end", "evalue", "bitscore"]
blastx_output = pandas.read_csv(path, sep = '\t', header = None, names = colnames)

#print(blastx_output)

# go through the query column and get the set of ORF names
ORFs = blastx_output['query'].tolist()
ORFs = set(ORFs)
top_hits = pandas.DataFrame(columns = colnames)
#print(ORFs)

# iterate over the ORF set
for o in ORFs:
    result_selection = blastx_output[blastx_output["query"] == o]
    # print(result_selection)
    # From those rows, choose the best result
    # based on bitscore (this syntax retrieves all rows if there are multiple rows with the maximum value)
    best_hit = result_selection[result_selection["bitscore"] == result_selection["bitscore"].max()]
    if len(best_hit) > 1 :
        # take smallest evalue if there is more than one hit with the maximum bitscore
        best_hit = best_hit[best_hit["evalue"] == best_hit["evalue"].min()]
        if len(best_hit) > 1 :
            # now take the longest alignment
            best_hit = best_hit[best_hit["length"] == best_hit["length"].max()]
            # if there are still multiple possible top hits, select the one with the higher percentage identity
            if len(best_hit) > 1 : 
                  max_pct = best_hit['pct ID'].max()
                  best_hit = best_hit[best_hit['pct ID'] == max_pct]


    #print(best_hit)
    top_hits = top_hits.append(best_hit)
    #### meaning the one with the highest bit score and lowest evalue
    #### if there are multiple, favour the one which has the largest overlap between query and reference sequence

print(top_hits)
filename = "/Users/martin/Internship/WesterdijkProject/output/blastx/filtered/GCF_000002495_results_1500_filtered.txt"
top_hits.to_csv(filename, sep = '\t', index = False, header = False)